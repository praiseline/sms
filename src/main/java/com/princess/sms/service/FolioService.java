package com.princess.sms.service;

import java.util.List;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
public interface FolioService {

  /**
   * 
   * @param bookingNumber
   * @param includeUsers
   * @param languageCode
   */
  public void printFolio(String bookingNumber, boolean includeUsers, String languageCode);


  /**
   * 
   * 
   * @param languageCode
   * @param bookingNumbers List<String>
   */
  public void printFolio(List<String> bookingNumbers, String languageCode);


}
