package com.princess.sms.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.princess.sms.model.Voyage;
import com.princess.sms.service.VoyageService;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
@RestController
public class VoyageController {

  @Autowired
  VoyageService voyageService;

  /**
   * Method findCurrentVoyage.
   * 
   * 
   * @return Voyage
   */
  @RequestMapping("/voyages/current")
  public Voyage findCurrentVoyage() {
    return voyageService.findCurrentVoyage();
  }
}
