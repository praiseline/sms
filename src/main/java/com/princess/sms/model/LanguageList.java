package com.princess.sms.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
@XmlRootElement(name = "languages")
public class LanguageList
    implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 4377418095944560630L;
  private int count;
  private List<Language> languages;

  public LanguageList() {

  }

  /**
   * Constructor for CabinList.
   * 
   * 
   * @param languages List<Language>
   */
  public LanguageList(List<Language> languages) {
    this.languages = languages;
    this.count = languages.size();
  }

  /**
   * Method getCount.
   * 
   * 
   * 
   * 
   * @return int
   */
  public int getCount() {
    return count;
  }

  /**
   * Method setCount.
   * 
   * @param count int
   */
  public void setCount(int count) {
    this.count = count;
  }

  /**
   * Method getLanguages.
   * 
   * 
   * @return List<Language>
   */
  @XmlElement(name = "language")
  public List<Language> getLanguages() {
    return languages;
  }

  /**
   * Method setLanguages.
   * 
   * @param languages List<Language>
   */
  public void setLanguages(List<Language> languages) {
    this.languages = languages;
  }

}
